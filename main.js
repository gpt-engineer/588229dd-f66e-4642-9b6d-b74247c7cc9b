document
  .getElementById("waitlist-form")
  .addEventListener("submit", function (e) {
    e.preventDefault();
    var email = document.getElementById("email").value;
    if (email) {
      document.getElementById("message").textContent =
        "Thank you for joining the waitlist. We will contact you at " +
        email +
        ".";
      document.getElementById("message").classList.add("text-green-500");
    } else {
      document.getElementById("message").textContent =
        "Please enter a valid email.";
      document.getElementById("message").classList.add("text-red-500");
    }
  });
